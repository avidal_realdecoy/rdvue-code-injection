# rdvue Code Injection

> For some of our upcoming features we will need to introduce some code injection for consistency of the code and full integration of the inserted modules.

---

## Usage

    > node index.js

---

## Example

The **auth** module being created will include inserting of:

* pages
* components
* services
* store-module (auth)

---
However there will also need to be updates to the following files which should already exists in the project:

    /src/store/index.ts
    /src/config/router.ts

---

## Method 1

### (Using code injection constants)

1. Reading the file to be updated (line by line).
2. Identifying a predetermined string (as a marker for the injection point).
3. Inserting the new block of code at that point in the file.
4. Writing the new file content back to the same file.

## Method 2

### (Using Regular Expressions & string comparisons)

1. Reading the file to be updated (as a whole).
2. Use RegExp to match against the entire file for one of the injection positions.
3. Use expected file structure to do string comparisons to find another injection position.
4. Inserting the new blocks of code at those points in the file.
5. Writing the new file content back to the same file.
