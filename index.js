const fs = require('fs');
const readline = require('readline');
const IMPORT_INJECTION_CONSTANT = '/* -- rdvue-import-injection -- */';
const ROUTE_INJECTION_CONSTANT = '/* -- rdvue-route-injection -- */';
const FILE_FORMAT = 'UTF-8';
const FILE_NAME = 'router.js';
const FILE_NAME_NO_CONSTANT = 'router-no-constant.js';
const NEW_ROUTE = {
    import: '\nimport NewView from \'@/page/new\';',
    route: ',\n{\n\tpath: \'/\',\n\tname: \'new\',\n\tcomponent: NewView,\n}'
}

function injectCodeUsingConstant(fileName, newRoute) {
    const rl = readline.createInterface({
        input: fs.createReadStream(fileName),
        output: process.stdout,
        terminal: false
    });

    // create variable for new file content
    let newFile = '';

    // on each line read
    rl.on('line', (line) => {
        if (line.includes(IMPORT_INJECTION_CONSTANT)) {
            newFile += newRoute.import;
        }
        if (line.includes(ROUTE_INJECTION_CONSTANT)) {
            newFile += newRoute.route;
        }
        newFile += `${ newFile.length > 0 ? '\n' : ''}${line}`
    });

    // overwrite file when file is closed
    rl.on('close', () => {
        fs.writeFile(fileName, newFile, (err) => {
            if (err) throw err;
            console.log('file updated using injection constants!');
        })
    });
}

// find list of imports and return position after the last import
function findImportInjectionPoint(data) {
    const lastImportRegex = new RegExp('^import [\*a-zA-Z{}\\-,\s]* from [@/\'a-zA-Z-]*;(?!.*^import [\*a-zA-Z{}\\-,\s]* from [@/\'a-zA-Z-]*;)', 'gms');
    const match = lastImportRegex.exec(data);
    return match.index + match[0].length;
}

// find list of routes and return position of the closing list bracket
function findRouteInjectionPoint(data) {
    const routesConst = "routes: ";
    const start = data.indexOf(routesConst);
    let openBracketCount = 0;
    let closeBracketCount = 0;
    let insertIndex = data.indexOf(routesConst) + routesConst.length;
    for (let i = start; i < data.length; i++) {
        if (data[i] == '[') {
            openBracketCount++;
        } else if (data[i] == ']') {
            closeBracketCount++;
        }
        // determine index to be the position before the matching closing bracket for the routes list
        if (openBracketCount > 0 && openBracketCount == closeBracketCount) {
            insertIndex = i - 1;
            break;
        }
    }
    return insertIndex;
}

function injectCodeWithoutConstant(fileName, newRoute) {
    try {
        // read contents of the file
        const data = fs.readFileSync(fileName, FILE_FORMAT);

        // find the position at which to inject the code
        const insertRouteIndex = findRouteInjectionPoint(data);
        const insertImportIndex = findImportInjectionPoint(data);

        // create new file content
        const dataArray = data.split('')
        let newFile = null;
        dataArray.splice(insertImportIndex, 0, newRoute.import);
        dataArray.splice(insertRouteIndex, 0, newRoute.route);
        newFile = dataArray.join('');

        // overwrite file with new content
        fs.writeFile(fileName, newFile, (err) => {
            if (err) throw err;
        })

        console.log('file updated without using injection constants!');
    } catch (err) {
        console.error(err);
    }
}

injectCodeUsingConstant(FILE_NAME, NEW_ROUTE);
injectCodeWithoutConstant(FILE_NAME_NO_CONSTANT, NEW_ROUTE);